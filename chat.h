#ifndef CHAT_H
#define CHAT_H

#define TRUE      			100000
#define FALSE    			100001
#define SUCCESS   			100002
#define FAILURE   			100003

/*****************************************************/
#define CMD_REGISTER   		1    		//注册
#define CMD_LOGIN      		2    		//登录
#define CMD_ONLINEUSER 		3    		//查看在线用户
#define CMD_PRIVATE    		4    		//私聊
#define CMD_GROUP      		5    		//群聊
#define CMD_FORBID     		6    		//禁言
#define CMD_KICKOUT  		7			//踢人
#define CMD_OFFLINE	   		8 			//下线
#define CMD_REGISTERVIP		9			//注册VIP
#define CMD_EXIT			10 			//退出程序
#define CMD_FILE 			11 			//传输文件
#define CMD_GETCHATINFO		12 			//获取聊天信息
/*****************************************************/



/*****************************************************/
#define RES_SUCCESS         10000 		//成功
#define RES_USEREXIST       10001 		//用户存在
#define RES_USERNOTEXIST    10002 		//用户不存在
#define RES_PASSWDERROR     10003 		//密码错误
#define RES_USEROFFLINE     10004		//用户不在线
#define RES_WAITINGONLINE   10005 		//等待接收在线用户信息
#define RES_NOTADMIN		10006		//不是管理员VIP
#define RES_FORBIDSUCCESS	10007		//禁言成功
#define RES_USERONLINE		10008		//用户在线
#define RES_MESSAGE			10009		//消息
#define RES_ASKEXIT			10010 		//下线
#define RES_VIPEXIST		10011 		//已经是VIP
#define RES_KICKOUT			10012 		//踢出成功
#define RES_USEREXIT 		10013		//用户退出登录
#define RES_EXIT 		 	10014 		//退出程序成功
#define RES_FILENAME 		10015 		//文件名称
#define RES_FILE 			10016 		//文件
#define RES_FILERECV		10017		//文件已接受
#define RES_FILEERROR		10018		//文件传输出错
#define RES_FILESEND		10019 		//文件已发送
#define RES_FILEFINSH		10020 		//文件结束
#define RES_CHATINFO		10021 		//聊天信息
#define RES_CHATINFOEND		10022		//聊天信息结束
/*****************************************************/


//客户端与服务器通信结构体
struct ChatInfo
{
	int cmd;							//命令
	char id[32]; 						//用户ID
	char passwd[32];					//用户密码
	char id_to[32]; 					//目标用户ID
	char text[128];						//文本消息
	int result; 						//服务器传回结果
	int admin_flag;						//VIP标志位
};
typedef struct ChatInfo Chat;

#endif
