#include "ClientHandler.h"


pthread_mutex_t mutex;		//互斥量声明

int admin_flag= 0;			//用户身份标志位

int flag_login = 0;			//用户登录标志位

int forbid_flag = 0;		//用户禁言标志位 

pthread_t tid[2] = {0};		//记录线程号

extern int sockfd;			//声明全局变量，文件描述符

char user_id[32] = {0};		//用户名

int msgid_recv;				//消息队列ID
int msgid_send;

int msgkey = 1234;			//消息队列键值初始化

/*
功能：主处理函数
形参：sockfd
返回值：无
 */
void main_handler()
{	
	int choice[32] = {0};

	//互斥锁初始化
	pthread_mutex_init(&mutex, NULL);

	while(1)
	{
		// printf("选择功能：1、注册 \t2、登录 \t3、退出:");
		menu1();
		fflush(stdout);
		fflush(stdin);
		scanf("%s", choice);

		switch(choice[0])
		{
			case '1':
				user_register();
				break;
			case '2':
				main_user_login();
				break;
			case '3':
				main_exit();
				break;
			default :
				break;
		}
		memset(choice, 0, sizeof(choice));
	}

	pthread_mutex_destroy(&mutex);	//摧毁互斥锁
}

/*
功能：程序退出函数
形参：无
返回值：无
 */	
void main_exit()
{
	int ret;
	Chat c_send, c_recv;

	strcpy(c_send.id, user_id);
	c_send.cmd = CMD_EXIT;

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("main_exit_send");
		exit(1);
	}

	ret = recv(sockfd, &c_recv, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("main_exit_recv");
		exit(1);
	}	

	if (c_recv.result == RES_EXIT)
	{
		printf("\t\t\t\033[1;31m退出中...\033[0m\n");
		sleep(1);
		close(sockfd);
		exit(1);
	}
	else
	{
		printf("ERROR!\n");
		sleep(1);
		close(sockfd);
		exit(1);
	}
	system("clear");
}
/*
功能：用户登录判断
形参：sockfd
返回值：无
 */
void login()
{
	// pthread_mutex_lock(&mutex);
	Chat c;
	int ret;

	printf("\t\t\t\033[1;31m请输入用户名：\033[0m");
	scanf("%s", c.id);
	printf("\t\t\t\033[1;31m请输入密码：\033[0m");
	scanf("%s", c.passwd);

	strcpy(user_id, c.id);
	c.cmd = CMD_LOGIN;
	// printf("sockfd= %d\n", sockfd);
	ret = send(sockfd, &c, sizeof(c), 0);   
	if (-1 == ret)
	{
		perror("login_send");
	}

	memset(&c, 0 ,sizeof(c));
	
	ret = recv(sockfd, &c, sizeof(c), 0);
	if (-1 == ret)
	{
		perror("login_recv");
	}	

	if (RES_USERNOTEXIST == c.result)
	{
		printf("\t\t\t\033[1;31m用户不存在！  \033[0m\n");
		sleep(1);
		flag_login = 0;
		return;
	}
	else if (RES_PASSWDERROR == c.result)
	{
		printf("\t\t\t\033[1;31m密码错误！  \033[0m\n");
		sleep(1);
		flag_login = 0;
		return;
	}
	else if (RES_USERONLINE == c.result)
	{
		printf("\t\t\t\033[1;31m用户已登录！ \033[0m\n");
		sleep(1);
		flag_login = 0;
		return;
	}
	else if (RES_SUCCESS == c.result)
	{
		printf("\t\t\t\033[1;31m登录成功！ \033[0m\n");
		sleep(1);
		flag_login = 1;
		admin_flag = c.admin_flag;
		return;
	}
	// pthread_mutex_unlock(&mutex);
}
/*
功能：获取在线用户信息
形参：1、id_to[100]首地址存放用户名(传出) 3、数组大小(传出)
返回值：无
 */
void GetOlineUser(char (*id_to)[32], unsigned int *length)
{
	// pthread_mutex_lock(&mutex);

	Chat c;
	struct msgbuf msg;
	int ret;
	
	c.cmd = CMD_ONLINEUSER;
	strcpy(c.id, user_id);

	ret = send(sockfd, &c, sizeof(c), 0);
	if (-1 == ret)
	{
		perror("getonlineuser_send");
		exit(1);
	}

	memset(&c, 0, sizeof(c));
	c.result = RES_WAITINGONLINE;

	// printf("获取在线用户信息中...\n");

	
	int i = 0;

	while(c.result == RES_WAITINGONLINE)
	{
		ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
		if (-1 == ret)
		{
			perror("getonlineuser_msgrcv");
			exit(1);
		}		

		memcpy(&c, &msg.c_msgbuf, sizeof(Chat));

		strcpy(id_to[i], c.id_to);
		if (c.result == RES_SUCCESS)
		{
			// printf("获取用户信息成功！\n");
			break;
		}

		i++;
		if(i > Size_Online)	//收100个
		{
			printf("\t\t\t\033[1;31m已经达到500人！ \033[0m\n");
			sleep(1);
			break;
		}
	}
	// pthread_mutex_unlock(&mutex);
	*length = (unsigned int)i;
}

/*
功能：显示所有在线用户信息（从数组中）
形参：1、id_to[100]首地址存放用户名 2、数组长度
返回值：无
 */
void ShowOnline(char (*id_to)[32], unsigned int length)
{
	int i = 0;	
	// printf("length = %d\n", length);
	while(length > 0)
	{
		printf("%s\n", id_to[i]);
		i++;
		length--;
	}
	sleep(3);
}

/*
功能：根据用户名私信
形参：无
返回值：无
 */
void PrivateMsg()
{
	if (1 == forbid_flag)
	{
		printf("\t\t\t\033[1;31m您已被禁言！ \033[0m\n");
		sleep(1);
		return;
	}

	Chat c, c_recv;
	int ret;
	struct msgbuf msg;

	memset(&c, 0, sizeof(c));
	c.cmd = CMD_PRIVATE;
	strcpy(c.id, user_id);
	printf("\t\t\t\033[1;31m我的昵称：%s\033[0m\n", c.id);
	printf("\t\t\t\033[1;31m请输入对方昵称: \033[0m");
	scanf("%s", c.id_to);

	while (1)
	{
		printf("\t\t\t\033[1;31m请输入聊天内容： \033[0m");
		scanf("%s",c.text);
		if (strcmp(c.text, "bye") == 0)
		{
			printf("\t\t\t\033[1;31m已退出聊天! \033[0m\n");
			sleep(1);
			break;
		}

		ret = send(sockfd, &c, sizeof(c), 0);
		if (-1 == ret)
		{
			perror("private_send");
			exit(1);
		}
		// printf("(%s)已发送！\n", c.text);

		ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
		if (-1 == ret)
		{
			perror("private_msgrcvs");
			exit(1);
		}		

		memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));

		if (c_recv.result == RES_SUCCESS)
		{
			printf("\t\t\t\033[1;31m发送成功！ \033[0m\n");
			// sleep(1);
		}
		else if (c_recv.result == RES_USEROFFLINE)
		{
			printf("\t\t\t\033[1;31m用户不在线！ \033[0m\n");
			// sleep(1);
			break;
		}
	}
}

/*
功能：群发
形参：1、id_to[100]首地址存放用户名 2、数组长度
返回值：无
 */
void GroupMsg(char (*id_to)[32], unsigned int length)
{
	if (1 == forbid_flag)
	{
		printf("\t\t\t\033[1;31m您已被禁言！ \033[0m\n");
		sleep(1);
		return;
	}

	Chat c_send, c_recv;
	int ret, j, i = 0;
	struct msgbuf msg;
	unsigned int length_tmp = length;
	char id_tmp[100][32] = {0};

	memcpy(id_tmp, id_to, sizeof(id_tmp));

	memset(&c_send, 0, sizeof(Chat));
	c_send.cmd = CMD_GROUP;
	strcpy(c_send.id, user_id);	

	while(1)
	{	
		memcpy(id_tmp, id_to, sizeof(id_tmp));
		i = 0;
		printf("\t\t\t\033[1;31m请输入聊天内容： \033[0m");
		scanf("%s",c_send.text);
		if (strcmp(c_send.text, "bye") == 0)
		{
			printf("\t\t\t\033[1;31m已退出群聊！ \033[0m\n");
			sleep(1);
			break;
		}
		length = length_tmp;
		// printf("length = %d\n", length);
		// for (j = 0; j < length; j++)
		// {
		// 	printf("%s  ", id_tmp[j]);
		// }
		
		// printf("\n");
		while (length > 1)	//length > 0 包括自己
		{	

			if (strcmp(id_tmp[i], user_id) == 0)	//不给自己发消息
			{
				i++;
				continue;
			}
			strcpy(c_send.id_to, id_tmp[i]);
			// printf("id_to = %s\n", c_send.id_to);
			strcmp(c_send.id, user_id);
			ret = send(sockfd, &c_send, sizeof(Chat), 0);
			if (-1 == ret)
			{
				perror("private_send");
				exit(1);
			}
			// printf("(%s)已发送！\n", c_send.text);

			ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
			if (-1 == ret)
			{
				perror("private_msgrcvs");
				exit(1);
			}		

			memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));


			if (c_recv.result == RES_SUCCESS)
			{
				printf("\t\t\t\033[1;31m%s:发送成功！内容：%s \033[0m\n", id_tmp[i], c_send.text);
				// sleep(1);
				i++;
				length--;
			}
			else if (c_recv.result == RES_USEROFFLINE)
			{
				printf("\t\t\t\033[1;31m%s:用户不在线！ \033[0m\n", id_tmp[i]);
				// sleep(1);
				i++;
				length--;
			}

			if (length <= 0)
			{
				printf("\t\t\t\033[1;31m群发完成！ \033[0m\n");
				// sleep(1);
			}	
		}
	}
}

/*
功能：注册会员
形参：无
返回值：无	
 */
void RegisterVIP()
{
	Chat c_send, c_recv;
	int ret;
	struct msgbuf msg;

	strcpy(c_send.id, user_id);
	c_send.cmd = CMD_REGISTERVIP;

	printf("\t\t\t\033[1;31m请输入激活码: \033[0m");
	scanf("%s", c_send.text);

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("registerVIP_send");
		exit(1);
	}

	ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
	if (-1 == ret)
	{
		perror("registerVIP_msgrcvs");
		exit(1);
	}		

	memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));

	if (c_recv.result == RES_NOTADMIN)
	{
		printf("\t\t\t\033[1;31m激活码无效！ \033[0m\n");
		sleep(1);
	}
	else if (c_recv.result == RES_VIPEXIST)
	{
		printf("\t\t\t\033[1;31m您已是会员，无需重新注册！ \033[0m\n");
		sleep(1);
		admin_flag = 1;
	}
	else if (c_recv.result == RES_SUCCESS)
	{
		printf("\t\t\t\033[1;31m[0m注册会员成功！ \033\n");
		sleep(1);
		admin_flag = 1;
	}
}

/*
功能：禁言处理函数
形参：无
返回值：无
 */
void ForbidUser()
{
	Chat c_send, c_recv;
	int ret;
	struct msgbuf msg;

	//判断是否为管理员
	if (admin_flag == 0)
	{
		printf("\t\t\t\033[1;31m \033[0m权限不足！\n");
		sleep(1);
		return;
	}

	strcpy(c_send.id, user_id);
	c_send.cmd = CMD_FORBID;
	printf("\t\t\t\033[1;31m请输入禁言对象: \033[0m");
	scanf("%s", c_send.id_to);

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("ForbidUser_send");
		exit(1);
	}
	ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
	if (-1 == ret)
	{
		perror("ForbidUser_msgrcvs");
		exit(1);
	}		

	memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));

	if (c_recv.result == RES_SUCCESS)
	{
		printf("\t\t\t\033[1;31m用户：%s已被禁言！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_VIPEXIST)
	{
		printf("\t\t\t\033[1;31m用户：%s为VIP，禁言失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_USEROFFLINE)
	{
		printf("\t\t\t\033[1;31m用户：%s不在线，禁言失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_USERNOTEXIST)
	{
		printf("\t\t\t\033[1;31m用户：%s不存在，禁言失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}

}
/*
功能：踢人下线
形参：无
返回值：无	
 */
void KickOutUser()
{
	Chat c_send, c_recv;
	int ret;
	struct msgbuf msg;

	//判断是否为管理员
	if (admin_flag == 0)
	{
		printf("\t\t\t\033[1;31m权限不足！ \033[0m\n");
		sleep(1);
		return;
	}

	strcpy(c_send.id, user_id);
	c_send.cmd = CMD_KICKOUT;
	printf("\t\t\t\033[1;31m请输入踢出对象昵称: \033[0m");
	scanf("%s", c_send.id_to);

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("KickOutUser_send");
		exit(1);
	}
	ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
	if (-1 == ret)
	{
		perror("KickOutUser_msgrcvs");
		exit(1);
	}		

	memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));

	if (c_recv.result == RES_SUCCESS)
	{
		printf("\t\t\t\033[1;31m用户：%s已被踢出！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_VIPEXIST)
	{
		printf("\t\t\t\033[1;31m用户：%s为VIP，踢出失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_USEROFFLINE)
	{
		printf("\t\t\t\033[1;31m用户：%s不在线，踢出失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
	else if (c_recv.result == RES_USERNOTEXIST)
	{
		printf("\t\t\t\033[1;31m用户：%s不存在，踢出失败！ \033[0m\n", c_send.id_to);
		sleep(1);
	}
}


/*
功能：登录系统线程id[0],回调函数
形参：NULL
返回值：无
 */
void *login_handler(void *arg)
{
	char choice[32] = {0};
	char id_to[Size_Online][32];	
	memset(&id_to, 0, sizeof(id_to));

	unsigned int length_online = 0;	//在线人数

	while(1)
	{
		// printf("主菜单：1、查看在线用户信息 2、私信 3、群发 4、禁言 5、踢人 6、注册会员 7、传送文件 8、获取聊天信息 9、下线\n");
		//printf("请选择功能:");
		menu2();
		scanf("%s", choice);

		switch(choice[0])
		{
			case '1':
				GetOlineUser(id_to, &length_online);	
				ShowOnline(id_to, length_online);		//显示在线用户
				break;
			case '2':
				GetOlineUser(id_to, &length_online);	
				PrivateMsg();							//私信
				break;
			case '3':
				GetOlineUser(id_to, &length_online);
				GroupMsg(id_to, length_online);			//群发
				break;
			case '4':
				GetOlineUser(id_to, &length_online);
				ForbidUser();							//禁言
				break;
			case '5':
				GetOlineUser(id_to, &length_online);
				KickOutUser();							//踢人
				break;
			case '6':
				RegisterVIP();							//注册会员
				break;
			case '7':
				GetOlineUser(id_to, &length_online);
				FileTransfer(id_to, length_online);
				break;
			case '8':
				GetUserMsg();
				break;
			case '9':
				UserExit();								//用户下线
				break;
			default :
				printf("\t\t\t\033[1;31m输入有误！ \033[0m\n");
				sleep(1);
				break;
		}
		memset(choice, 0, sizeof(choice));
	}
	return NULL;
}

void GetUserMsg()
{
	Chat c_send, c_recv;
	int ret;
	struct msgbuf msg;

	c_send.cmd = CMD_GETCHATINFO;
	strcpy(c_send.id, user_id);

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("GetUserMsg_send");
		exit(1);
	}

	while (1)
	{
		ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
		if (-1 == ret)
		{
			perror("KickOutUser_msgrcvs");
			exit(1);
		}		

		memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));
		if (c_recv.result == RES_CHATINFO)
		{
			printf("\t\t\t\033[1;31m%s \033[0m\n", c_recv.text);
		}

		if (c_recv.result == RES_CHATINFOEND)
		{
			printf("\t\t\t\033[1;31m聊天信息接收完毕！ \033[0m\n");
			sleep(4);
			break;
		}
	}
}

void FileTransfer(char (*id_to)[32], unsigned int length)
{
	Chat c_send, c_recv;
	int ret, i = 0;
	struct msgbuf msg;
	char filename[32] = {0};

	strcpy(c_send.id, user_id);
	system("ls -l");
	printf("\t\t\t\033[1;31m输入文件名： \033[0m");
	scanf("%s", filename);
	while (1)
	{
		printf("\t\t\t\033[1;31m输入传输用户昵称： \033[0m");
		scanf("%s", c_send.id_to);	
		if (strcmp(c_send.id_to, id_to[i++]) == 0 || i >= length)
		{
			break;
		}
	}

	if (i >= length)
	{
		printf("\t\t\t\033[1;31m对方不在线！ \033[0m\n");
		sleep(1);
		return;
	}


	//以只读方式打开该文件 如果文件不存在 ret = -1
	int fd = open(filename, O_RDONLY);
	if (-1 == ret)
	{
		printf("\t\t\t\033[1;31m文件不存在！ \033[0m\n");
		sleep(1);
		return;
	}	
	c_send.cmd = CMD_FILE;
	c_send.result = RES_FILENAME;
	// printf("FileName = %s\n", filename);

	strcpy(c_send.text, filename);
	ret = send(sockfd, &c_send, sizeof(c_send), 0);
	if(-1 == ret)
	{
		perror("FileTransfer_send1");
		exit(1);
	}

	while(1)
	{
		//每次从文件读取 sizeof(buf) - 1 字节， 不传输末尾 '\0'
		ret = read(fd, c_send.text, sizeof(c_send.text) - 1);
		if (-1 == ret)
		{
			perror("read");
			exit(1);
		}
		else if(0 == ret)	//文件读完 
		{
			c_send.result = RES_FILEFINSH;
			ret = send(sockfd, &c_send, sizeof(c_send), 0);
			if(-1 == ret)
			{
				perror("FileTransfer_send2");
				exit(1);
			}
			printf("\t\t\t\033[1;31m文件传输完毕 ! \033[0m\n");
			sleep(1);
			break;
		}
		c_send.result = RES_FILE;
		//将读取的字节传输给服务器
		ret = send(sockfd, &c_send, sizeof(c_send), 0);
		if(-1 == ret)
		{
			perror("FileTransfer_send2");
			exit(1);
		}
		memset(c_send.text, 0, sizeof(c_send.text));
	}
	close(fd);
	// ret = msgrcv(msgid_recv, &msg, sizeof(Chat), 1 , 0);
	// if (-1 == ret)
	// {
	// 	perror("FileTransfer_msgrcvs");
	// 	exit(1);
	// }		

	// memcpy(&c_recv, &msg.c_msgbuf, sizeof(Chat));

	// if (c_recv.result == RES_FILESEND)
	// {
	// 	printf("文件传输完毕！\n");
	// }
	// else if (c_recv.result == RES_FILEERROR)
	// {
	// 	printf("文件传输出错！\n");
	// }

}
/*
功能：用户下线处理函数：返回到登陆界面
形参：无
返回值：无
 */
void UserExit()
{
	Chat c_send;
	int ret;
	struct msgbuf msg;

	c_send.cmd = CMD_OFFLINE;
	c_send.result = RES_ASKEXIT;
	strcpy(c_send.id, user_id);

	ret = send(sockfd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("UserExit_send");
		exit(1);
	}	
}

/*
功能：接收线程tid[1]回调函数，专门接收消息的线程
形参：NULL
返回值：无
 */
void *recv_handler(void *arg)
{
	// int sockfd = *(int *)arg;
	int ret;
	Chat c_recv;

	struct msgbuf MsgBuf;
	memset(&MsgBuf, 0, sizeof(MsgBuf));
	memset(&c_recv, 0, sizeof(Chat));

	while (1)
	{
		// pthread_mutex_lock(&mutex);
		// printf("sockfd == %d\n", sockfd);sss
		ret = recv(sockfd, &c_recv, sizeof(Chat), 0);
		if (-1 == ret)
		{
			perror("recv_handler_recv");
			msgctl(msgid_recv, IPC_RMID, NULL);			//关闭消息队列
			pthread_cancel(tid[0]);						//关闭登录线程
			pthread_exit((void*)1);						//关闭接收线程	
			exit(1);
		}

		// printf("线程收到 result = %d c_recv.cmd = %d \n", c_recv.result, c_recv.cmd);
		if (RES_MESSAGE == c_recv.result)				//收到消息，显示
		{	//\t\t\t\033[1;31m \033[0m
			printf("\t\t\t\t\t\033[1;35m%s:来自(%s) \033[0m\n", c_recv.text, c_recv.id);
			sleep(2);
		}
		else if (RES_FORBIDSUCCESS == c_recv.result)	//告知被禁言处理
		{
			forbid_flag = 1;
			printf("\t\t\t%s\n", c_recv.text);
		}
		else if (RES_KICKOUT == c_recv.result)			//告知被踢下线处理，返回到登陆界面
		{
			flag_login = 0;
			memset(user_id, 0, sizeof(user_id));
			printf("\t\t\t%s\n", c_recv.text);

			msgctl(msgid_recv, IPC_RMID, NULL);			//关闭消息队列
			pthread_cancel(tid[0]);						//关闭登录线程
			pthread_exit((void*)1);						//关闭接收线程
		}
		else if (CMD_FILE == c_recv.cmd)				//接收文件
		{
			RecvFile(&c_recv);
		}
		else if(0 == c_recv.result)						//非预期值处理
		{
			memset(&MsgBuf, 0, sizeof(MsgBuf));
			memset(&c_recv, 0, sizeof(Chat));

			continue;
		}
		else if (RES_USEREXIT == c_recv.result)		//退出登录处理
		{
			printf("\n\t\t\t\033[1;31m退出登录中... \033[0m\n");
			flag_login = 0;
			memset(user_id, 0, sizeof(user_id));		//清空用户名称数组
			sleep(1);

			msgctl(msgid_recv, IPC_RMID, NULL);			//关闭消息队列
			pthread_cancel(tid[0]);						//关闭登录线程
			pthread_exit((void*)1);						//关闭接收线程			
		}
		else											//需要另一个线程处理的结果
		{	
			// printf("线程收到并转发 result %d id_to = %s\n", c_recv.result, c_recv.id_to);
			// printf("线程收到并转发: %s\n", c_recv.text);

			memcpy(&MsgBuf.c_msgbuf, &c_recv, sizeof(Chat));

			MsgBuf.mtype = 1;
			ret = msgsnd(msgid_send, &MsgBuf, sizeof(Chat), 0);		//通过消息队列，转发给另一个线程
			if (-1 == ret)
			{
				perror("recv_handler_msgsend");
				exit(1);
			}
		}

		memset(&MsgBuf, 0, sizeof(MsgBuf));
		memset(&c_recv, 0, sizeof(Chat));
		// pthread_mutex_unlock(&mutex);
		usleep(10000);	//100ms
	}

	return NULL;
}

void RecvFile(Chat *c_recv)
{
	Chat s_end;
	int fd_recv, ret;
	static char filename[32];

	if (RES_FILENAME == c_recv->result)
	{
		strcpy(filename, c_recv->text);
		// printf("*****filename = %s\n", filename);
	}	
	else if (RES_FILE == c_recv->result)
	{
		// printf("filename = %s\n", filename);
		fd_recv = open(filename, O_CREAT | O_WRONLY | O_APPEND, 00700);
		if (-1 == ret)
		{
			printf("FILE CREATE ERROR！\n");
		}
		ret = write(fd_recv, c_recv->text, strlen(c_recv->text));
		if (-1 == ret)
		{
			perror("write");
			exit(1);
		}

		close(fd_recv);
	}
	else if (RES_FILEFINSH == c_recv->result)
	{
		printf("\t\t\t\033[1;31m文件<%s>接收成功！ \033[0m\n", filename);
		sleep(1);
	}
}
/*
功能：用户登录主系统，创建双线程
形参：无
返回值：无
 */
void main_user_login()
{
	int ret;
	srand(time(NULL));


	login();							//用户登录

	// printf("flag_login = %d\n", flag_login);
	// printf("admin_flag = %d\n", admin_flag);

	if(flag_login == 1)					//登录成功
	{
		// msgkey = ftok("ftok", 666);	//获取键值
		msgkey = rand();				//键值不能重复，否则同一设备冲突，消息队列

		//创建消息队列	
		msgid_send = msgget(msgkey, IPC_CREAT | IPC_EXCL); //IPC_EXCL
		if (-1 == msgid_send)
		{
			perror("main_user_login_msgget1");
			exit(1);
		}

		msgid_recv = msgget(msgkey, 0); 
		if (-1 == msgid_recv)
		{
			perror("main_user_login_msgget2");
			exit(1);
		}

		// printf("msgid_send = %d\n", msgid_send);
		// printf("msgid_recv = %d\n", msgid_recv);

		//登录系统线程
		ret = pthread_create(&tid[0], NULL, login_handler, NULL);
		if (-1 == ret)
		{
			perror("login_pthread_create");
		}

		//接收消息线程
		ret = pthread_create(&tid[1], NULL, recv_handler, NULL);
		if (-1 == ret)
		{
			perror("login_pthread_create");
		}

		//阻塞进程	回收线程资源
		void *status;
		pthread_join(tid[0], &status);
		pthread_join(tid[1], &status);
	}

}

/*
功能：用户注册
形参：无
返回值：无
 */
void user_register()
{
	// pthread_mutex_lock(&mutex);
	int ret;
	Chat c;

	memset(&c, 0 ,sizeof(c));
	printf("\t\t\t\033[1;31m输入用户名: \033[0m");
	scanf("%s", c.id);
	printf("\t\t\t\033[1;31m输入密码: \033[0m");
	scanf("%s", c.passwd);
	c.cmd = CMD_REGISTER;

	ret = send(sockfd, &c, sizeof(c), 0);   
	if (-1 == ret)
	{
		perror("user_register_send");
		exit(1);
	}

	memset(&c, 0 ,sizeof(c));
	ret = recv(sockfd, &c, sizeof(c), 0);
	if (-1 == ret)
	{
		perror("user_register_recv");
		exit(1);
	}	

	// printf("result = %d", c.result);
	if (c.result == RES_SUCCESS)
	{
		printf("\t\t\t\033[1;31m注册成功! \033[0m\n");
		sleep(1);
	}
	else if (c.result == RES_USEREXIST)
	{
		printf("\t\t\t\033[1;31m账户已经存在，注册失败...... \033[0m\n");
		sleep(1);
	}

	// pthread_mutex_unlock(&mutex);
}

