#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <time.h>
#include <unistd.h>
#include "../chat.h"
#include "ClientSocket.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define Size_Online   500 	//存储在线人数数组上限500

//线程通信结构体
struct msgbuf
{
	long mtype;		//通信类型(固定)
	Chat c_msgbuf;	//通信内容
};

void main_handler();
void user_register();
void main_user_login();
void main_exit();
void login();
void *login_handler(void *arg);
void *recv_handler(void *arg);
// void GotMessage(Chat *c);
void GetOlineUser(char (*id_to)[32], unsigned int *length);
void ShowOnline(char (*id_to)[32], unsigned int length);
void UserExit();
void PrivateMsg();
void GroupMsg(char (*id_to)[32], unsigned int length);
void RegisterVIP();
void ForbidUser();
void KickOutUser();
void FileTransfer();
void RecvFile(Chat *s_recv);
void GetUserMsg();

#endif

