#include "ClientSocket.h"

int sockfd = 0;	//全局变量
/*
功能：连接服务器
形参：无
返回值：sockfd
 */
int InitSocket()
{
	// int sockfd;
	struct sockaddr_in server_addr;

	//建立sockfd
	sockfd = socket(PF_INET, SOCK_STREAM, 0);
	if (-1 == sockfd)
	{
		perror("socket");
		exit(1);
	}
	// printf("初始化sockfd = %d\n", sockfd);
	//初始化服务器信息
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = PF_INET;
	server_addr.sin_port = 8000;
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	//连接服务器
	int ret = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (-1 == ret)
	{
		perror("connect");
		exit(1);
	}

	return sockfd;
}


void menu1()
{
	system("clear");			//\033[1;34m  \033[0m
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m==================== \033[0m\033[1m\033[5;34m 智障聊天室 \033[0m\033[1m\033[1;34m ====================== \033[0m\n");
	printf("\t\t\t                  \033[1;35m 1、用户注册 \033[0m\n");
	printf("\t\t\t                  \033[1;35m 2、用户登录 \033[0m\n");
	printf("\t\t\t                  \033[1;35m 3、退出 \033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;31m请选择功能\033[0m \033[1m\033[5;31m ： \033[0m");
	sleep(1);
}

void menu2()
{
	system("clear");			//\033[1;34m  \033[0m
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m====================== \033[0m\033[1m\033[1;34m 主菜单 \033[0m\033[1m\033[1;34m ======================== \033[0m\n");
	printf("\t\t\t         \033[1;35m 1、查看在线用户信息    2、私信\033[0m\n");
	printf("\t\t\t         \033[1;35m 3、群发                4、禁言 \033[0m\n");
	printf("\t\t\t         \033[1;35m 5、踢人                6、注册会员 \033[0m\n");
	printf("\t\t\t         \033[1;35m 7、传送文件            8、获取聊天记录 \033[0m\n");
	printf("\t\t\t         \033[1;35m 9、下线 \033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;34m========================================================\033[0m\n");
	printf("\t\t\t\033[1m\033[1;31m请选择功能\033[0m \033[1m\033[5;31m ： \033[0m");
	sleep(1);	
}


