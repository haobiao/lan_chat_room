#include "database.h"

/*
功能：初始化数据库，创建两张表，用户信息和聊天信息
形参：无
返回值：无
 */
void InitDatabase()
{
	sqlite3 *ppdb;
	char sql[1024] = {0};
	char admin[32] = "admin";	//默认管理员用户

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		printf("sqlite3_open1 : %s\n", sqlite3_errmsg(ppdb));
	}

	//创建账户信息表
	sprintf(sql, "create table if not exists UserInfo (id text, password text, admin_flag integer);");
	ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);
	if (ret != SQLITE_OK)
	{
		printf("sqlite3_exec1 : %s\n", sqlite3_errmsg(ppdb));
	}

	if ( FALSE == if_user_exist(admin))	//判断数据库是否有admin用户
	{
		sprintf(sql, "insert into UserInfo values ('%s', 'admin', 1);", admin);
		ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);		
	}

	
	//创建聊天信息表
	sprintf(sql, "create table if not exists ChatInfo (id_from text, id_to text, time text, message text);");
	ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);
	if (ret != SQLITE_OK)
	{
		printf("sqlite3_exec1 : %s\n", sqlite3_errmsg(ppdb));
	}	


	sqlite3_close(ppdb);
}

/*
功能：回调函数 判断是否有结果，被调用则存在
 */
int flag = 0;
int callback(void *para, int columnCount, char **columnValue, char **columnName)
{
	flag = 1;

	return 0;
}

/*
功能：判断数据库是否有该用户
形参：用户ID
返回值：有：TRUE 无：FALSE
 */
int if_user_exist(char *id)
{
	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "select id from UserInfo where id = '%s';", id);
	ret = sqlite3_exec(ppdb, sql, callback, NULL, NULL);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sqlite3_close(ppdb);

	if (0 == flag)    //回调函数没有被调用、用户不存在
	{
		return FALSE;
	}
	else
	{
		flag = 0;
		return TRUE;
	}
}

/*
功能：判断数据库是否有与用户名称匹配的密码
形参：1、用户名称 2、用户passwd
返回值：有：TRUE 无：FALSE 失败：FAILURE
 */
int if_passwd_exist(char *id, char *passwd)
{
	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "select password from UserInfo where password = '%s' and id = '%s';", passwd, id);
	ret = sqlite3_exec(ppdb, sql, callback, NULL, NULL);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sqlite3_close(ppdb);

	if (0 == flag)    //回调函数没有被调用、用户不存在
	{
		return FALSE;
	}
	else
	{
		flag = 0;
		return TRUE;
	}
}

/*
功能：判断用户是否为管理员
形参：用户名称
返回值：有：TRUE 无：FALSE  失败：FAILURE
 */
int if_user_admin(char *id)
{
	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "select admin_flag from UserInfo where id = '%s' and admin_flag = 1;", id);
	ret = sqlite3_exec(ppdb, sql, callback, NULL, NULL);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sqlite3_close(ppdb);

	if (0 == flag)    //回调函数没有被调用、用户不存在
	{
		return FALSE;
	}
	else
	{
		flag = 0;
		return TRUE;
	}
}

/*
功能：向数据库写入用户信息
形参：Chat结构体变量地址
返回值：成功：SUCCESS 失败：FAILURE
 */
int write_database_userinfo(Chat *c)
{
	if (NULL == c)
	{
		return FAILURE;
	}

	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "insert into UserInfo values ('%s', '%s', 0);", c->id, c->passwd);
	ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);
	if (ret != SQLITE_OK)
	{
			return FAILURE;
	}

	sqlite3_close(ppdb);

	return SUCCESS;

}

/*
功能：将聊天信息存入结构体：发送用户ID、接收用户ID、发送时间、消息内容
形参：1、Chat结构体变量地址 2、时间字符串  2019/1/1 12:00:00
返回值：成功：SUCCESS 失败：FAILURE
 */
int write_database_ChatInfo(Chat *c, char *time)
{
	if (NULL == c)
	{
		return FAILURE;
	}

	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "insert into ChatInfo values ('%s', '%s', '%s', '%s');", c->id, c->id_to, time, c->text);
	ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);
	if (ret != SQLITE_OK)
	{
			return FAILURE;
	}

	sqlite3_close(ppdb);

	return SUCCESS;

}

/*
功能：将用户VIP标志位写入数据库
形参：在线用户结点地址
返回值：成功：SUCCESS 失败：FAILURE
 */
int write_database_admin_flag(Online *Node)
{
	if (NULL == Node)
	{
		return FAILURE;
	}

	sqlite3 *ppdb;
	char sql[128] = {0};

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}

	sprintf(sql, "update UserInfo set admin_flag = %d where id = '%s';", Node->admin_flag, Node->id);
	ret = sqlite3_exec(ppdb, sql, NULL, NULL, NULL);
	if (ret != SQLITE_OK)
	{
			return FAILURE;
	}

	sqlite3_close(ppdb);

	return SUCCESS;

}



int get_chatinfo(void *para, int columnCount, char **columnValue, char **columnName)
{
	int fd = *(int *)para;

	int ret;
	Chat c_send;
	// char msg_text[128] = {0};
	sprintf(c_send.text, "%s:%s %s:%s time:%s message:%s", 
		columnName[0],columnValue[0], columnName[1],columnValue[1], columnValue[2], columnValue[3]);

	// printf("text:%s\n", c_send.text);

	c_send.result = RES_CHATINFO;
	ret = send(fd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("get_chatinfo_send");
		exit(1);
	}
	return 0;	//不加此,函数只能执行一次	！！！！
}

int get_database_chatinfo(int fd, char *id)
{
	if (NULL == id)
	{
		return FAILURE;
	}

	sqlite3 *ppdb;
	char sql[128] = {0};
	Chat c_send;

	int ret = sqlite3_open("ChatRoomInfo.db", &ppdb);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}
	// printf("******************\n");
	sprintf(sql, "select * from ChatInfo where id_from = '%s' or id_to = '%s';", id, id);
	ret = sqlite3_exec(ppdb, sql, get_chatinfo, (void *)&fd, NULL);
	if (ret != SQLITE_OK)
	{
		return FAILURE;
	}
	//select * from ChatInfo where id_from = "asd" or id_to = "asd";

	// printf("*****************\n");
	//发送结束
	printf("获取聊天信息结束！\n");

	c_send.result = RES_CHATINFOEND;
	ret = send(fd, &c_send, sizeof(Chat), 0);
	if (-1 == ret)
	{
		perror("get_chatinfo_send");
		exit(1);
	}
	
	sqlite3_close(ppdb);
	return SUCCESS;
}
