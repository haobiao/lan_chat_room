#ifndef DATABASE_H
#define DATABASE_H

#include "../chat.h"
#include "link.h"
#include <stdio.h>
#include <sqlite3.h>


void InitDatabase();
int write_database_userinfo(Chat *c);
int callback(void *para, int columnCount, char **columnValue, char **columnName);
int if_user_exist(char *id);
int if_passwd_exist(char *id, char *passwd);
int if_user_admin(char *id);
int write_database_ChatInfo(Chat *c, char *time);
int write_database_admin_flag(Online *Node);
int get_database_chatinfo(int fd, char *id);


#endif
