#include "link.h"


/*
功能：初始化在线用户信息链表
形参：头结点地址
返回值：成功：SUCCESS 失败：FAILURE
 */
int InitLink(Online **h)
{
	if (NULL == h)
	{
		return FAILURE;
	}

	(*h) = (Online *)malloc(sizeof(Online));
	if (NULL == (*h))
	{
		printf("malloc failure!\n");
	}

	(*h)->next = NULL;
	(*h)->status = 0;
	
	return SUCCESS;
}

/*
功能：向链表插入一个结点
形参：1、头结点 2、位置place 3、需要插入结点的地址
返回值：成功：SUCCESS 失败：FAILURE
 */
int InsertLink(Online *head, int place, Online *element)
{
	if (NULL == head || NULL == element)
	{
		return FAILURE;
	}

	Online *l = head;

	if (place <= 0 || place > head->status + 1)
	{
		return FAILURE;
	}

	int k = 1;
	while (k < place)
	{
		k++;
		l = l->next;
	}

	element->next = l->next;
	l->next = element;


	head->status++;	//链表长度

	return SUCCESS;
}

/*
功能：遍历链表
形参：头结点地址
返回值：成功：SUCCESS 失败：FAILURE
 */
int TraverseLink(Online *head)
{
	if (NULL == head)
	{
		return FAILURE;
	}

	Online *l = head;

	while(l->next != NULL)
	{
		l = l->next;
		printf("id = %s fd = %d forbid = %d admin = %d status = %d\n", 
			l->id, l->fd, l->forbid_flag, l->admin_flag, l->status);

	}

	return SUCCESS;
}

/*
功能：根据用户ID返回该用户的结点地址
形参：1、头结点 2、用户ID
返回值：有：返回该结点地址 无：返回：NULL
 */
Online *GetNodeFromID(Online *head, char *user)
{
	if(NULL == user || NULL == head)
	{
		return NULL;
	}

	Online *l = head->next;

	while(NULL != l)
	{
		if (strcmp(user, l->id) == 0)
		{
			return l;
		}
		l = l->next;
	}

	return NULL;
}

/*
功能：根据用户ID，删除结点信息
形参：1、头结点 2、用户ID
返回值：成功：SUCCESS 失败：FAILURE
 */
int DeleteNodeFromID(Online *head, char *user)
{
	if(NULL == user || NULL == head)
	{
		return FAILURE;
	}

	Online *l = head;
	Online *tmp;

	while(NULL != l->next)
	{	
		tmp = l;
		l = l->next;
		if (strcmp(user, l->id) == 0)
		{

			break;
		}
	}

	if (NULL == l)
	{
		return FAILURE;
	}

	tmp->next = l->next;

	head->status--;
	free(l);

	return SUCCESS;
}




