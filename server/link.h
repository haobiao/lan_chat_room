#ifndef LINK_H
#define LINK_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../chat.h"

//在线用户结点信息
struct OnlineNode
{
	char id[32];
	int fd;
	int forbid_flag;
	int admin_flag;
	int status;
	struct OnlineNode *next;
};
typedef struct OnlineNode Online;

int InitLink(Online **h);
int InsertLink(Online *head, int place, Online *element);
int TraverseLink(Online *head);
Online *GetNodeFromID(Online *head, char *user);
int DeleteNodeFromID(Online *head, char *user);


#endif
