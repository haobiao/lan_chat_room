#include "socket.h"
#include "database.h"
#include "link.h"

int main()
{
	int sockfd;
	Online *head;

	sockfd = InitNet();

	InitDatabase();

	InitLink(&head);

	MainHandler(sockfd, head);

	return 0;
}
