#ifndef SOCKET_H
#define SOCKET_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include "link.h"
#include "../chat.h"
#include "database.h"

//线程回调函数传输多个参数结构体
struct head_fd_struct
{
	Online *head_s;
	int fd_s;
};

int InitNet();
void GetTime(char *str_time);
void MainHandler(int sockfd, Online *head);
void onlineuser_handler(Chat *c_recv, int fd, Online *head);
void private_handler(int fd, Chat *c, Online *head);
void userexit(int fd, Chat *c, Online *head);
void group_handler(int fd, Chat *c, Online *head);
void register_vip(int fd, Chat *c, Online *head);
void forbid_user_handler(int fd, Chat *c, Online *head);
void kick_out_user_handler(int fd, Chat *c, Online *head);
void main_exit(int fd, Chat *c, Online *head, Online *Node_tmp);
void file_transfer(int fd, Chat *c, Online *head);

#endif
